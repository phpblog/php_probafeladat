<?php
    class Topic_model extends CI_Model
    {
        public function __construct()
        {
            $this->load->database();
        }

        public function get_topics($slug = FALSE)
        {
            if($slug === FALSE)
            {
                $this->db->order_by('topic_id', 'DESC');
                $this->db->limit(4);
                $this->db->join('category', 'category.category_id = topic.category_id');
                $query = $this->db->get('topic');
                return $query->result_array();
            }

            $query = $this->db->get_where('topic', array('slug' => $slug));
            return $query ->row_array();
        }

        public function create_topic()
        {
            $slug = url_title($this->input->post('topic_name'));

            $data = array(
                'topic_name' => $this->input->post('topic_name'),
                'slug' => $slug,
                'topic_body' => $this->input->post('topic_body'),
                'category_id' => $this->input->post('category_id'),
                'user_id' => $this->session->userdata('user_id')
            );

            return $this->db->insert('topic', $data);
        }

        public function delete_topic($topic_id)
        {
            $this->db->where('topic_id', $topic_id);
            $this->db->delete('topic');
            return true;
        }

        public function update_topic()
        {
            $slug = url_title($this->input->post('topic_name'));

            $data = array(
                'topic_name' => $this->input->post('topic_name'),
                'slug' => $slug,
                'topic_body' => $this->input->post('topic_body'),
                'category_id' => $this->input->post('category_id')
            );

            $this->db->where('topic_id', $this->input->post('topic_id'));
            return $this->db->update('topic', $data); 
        }

        public function get_categories()
        {
            $this->db->order_by('category_name');
            $query = $this->db->get('category');
            return $query->result_array();
        }

        public function get_topics_by_category($category_id)
        {
            $this->db->order_by('topic_id', 'DESC');
            $this->db->join('category', 'category.category_id = topic.category_id');
            $query = $this->db->get_where('topic', array('category.category_id' => $category_id));
            return $query->result_array();
        }
    }
?>