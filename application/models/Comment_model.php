<?php 
    class Comment_model extends CI_Model
    {
        public function __construct()
        {
            $this->load->database();
        }

        public function create_comment($topic_id)
        {
            $data = array(
                'topic_id' => $topic_id,
                'comment_body' => $this->input->post('comment'),
                'user_id' => $this->input->post('user_id')
            );

            return $this->db->insert('comment', $data);
        }

        public function get_comments($topic_id)
        {
            $this->db->join('user', 'user.user_id = comment.user_id');
            $query = $this->db->get_where('comment', array('topic_id' => $topic_id));
            return $query->result_array();
        }
    }
?>