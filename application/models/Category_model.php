<?php
    class Category_model extends CI_Model
    {
        public function __construct()
        {
            $this->load->database();
        }

        public function get_categories()
        {
            $this->db->order_by('category_name');
            $query = $this->db->get('category');
            return $query->result_array();
        }

        public function get_category($id)
        {
            $query = $this->db->get_where('category', array('category_id' => $id));
            return $query->row();
        }
    }
?>