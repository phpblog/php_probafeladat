<?php 
    class User_model extends CI_Model
    {
        public function register($enc_password)
        {
            $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'password'=> $enc_password,
                'premission' => 'superadmin'
            );

            return $this->db->insert('user', $data);
        }

        public function login($email, $password)
        {
            $this->db->where('email', $email);
            $this->db->where('password', $password);

            $result = $this->db->get('user');

            if($result->num_rows() == 1)
            {
                return $result->row(0)->user_id;
            }
            else
            {
                return false;
            }
        }

        public function check_username_exists($username)
        {
            $query = $this->db->get_where('user', array('username' => $username));
            if(empty($query->row_array()))
            {
                return true;
            }else  
            {
                return false;
            }
        }

        public function check_email_exists($email)
        {
            $query = $this->db->get_where('user', array('email' => $email));
            if(empty($query->row_array()))
            {
                return true;
            }else  
            {
                return false;
            }
        }

        public function get_user($user_id)
        {
            $query = $this->db->get_where('user', array('user_id' => $user_id));
            return $query->row_array();
        }

        public function update_profile($password)
        {
            $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'password' => $password
            );

            $this->db->where('user_id', $this->session->userdata('user_id'));
            return $this->db->update('user', $data); 
        }

        public function get_users()
        {
            $query = $this->db->get('user');
            return $query ->result_array();
        }
    }
?>