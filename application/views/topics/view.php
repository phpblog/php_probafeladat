<h2><?php echo $topic['topic_name']; ?></h2>
<small class="topic-date">Posted on: <?php echo $topic['topic_date']; ?></small><br>
<div class="topic-body">
    <?php echo $topic['topic_body']; ?>
</div>
<!-- logged in then can edit delete -->
<?php if($this->session->userdata('user_id') == $topic['user_id']): ?>
    <hr> 
    <div class="row">
        <div class="col-md-0">
            <a class="btn btn-success pull-left" href="edit/<?php echo $topic['slug']; ?>">Edit</a>
        </div>
        <div class="col-md-0">
            <?php echo form_open('/topics/delete/'.$topic['topic_id']);?>
                <input type="submit" value="Delete" class="btn btn-danger">
            </form>
        </div>
    </div>
<?php endif; ?>

<h3>Comments</h3>
<?php if($comments) : ?>
        <?php foreach($comments as $comment) : ?>
            <div class="card bg-light p-3">
                <small class="topic-date">Commented on: <?php echo $comment['comment_date']; ?><br> by <strong><?php echo $comment['username']; ?></strong></small><br>
                <p><?php echo $comment['comment_body']; ?></p>
            </div>
        <?php endforeach; ?>
    <?php else : ?>
        <p>No comments to display</p>
<?php endif; ?>

<!-- logged in then can comment -->
<?php if($this->session->userdata('logged_in')): ?>
    <h3>Add Comment</h3>
    <?php echo validation_errors(); ?>
    <?php echo form_open('comments/create/'.$topic['topic_id']); ?>
        <div class="form-group">
            <textarea name="comment" class="form-control"></textarea>
        </div>
        <input type="hidden" name="slug" value="<?php echo $topic['slug']; ?>">
        <input type="hidden" name="user_id" value="<?php echo $this->session->userdata('user_id'); ?>">
        <button class="btn btn-success" type="submit">Submit</button>
    </form>
<?php endif; ?>
