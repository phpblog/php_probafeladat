<h2><?= $title; ?></h2>

<?php echo validation_errors(); ?>

<?php echo form_open('topics/create'); ?>
  <div class="form-group">
    <label>Topic name</label>
    <input type="text" class="form-control" name="topic_name" placeholder="Add Topic Name">
  </div>
  <div class="form-group">
    <label>Topic body</label>
    <textarea id="editor1" class="form-control" name="topic_body" placeholder="Type here..."></textarea>
  </div>
  <div class="form-group">
  <label>Category</label>
  <select name="category_id" class="form-control">
    <?php foreach($categories as $category): ?>
      <option value="<?php echo $category['category_id']; ?>"><?php echo $category['category_name']; ?></option>
    <?php endforeach; ?>
  </select>
  </div> 
  <button type="submit" class="btn btn-success">Submit</button>
</form>