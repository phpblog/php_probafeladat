<h2><?= $title ?></h2>
<?php foreach($topics as $topic) : ?>
    <h3><?php echo $topic['topic_name']; ?></h3>
    <div class="row">
    <div class="col-md-3">
    <img class="topic-thumb" src="<?php echo site_url(); ?>assets/images/<?php echo $topic['category_image']; ?>">
    </div>
    <div class="col-md-9">
        <small class="topic-date">Posted on: <?php echo $topic['topic_date']; ?> in <strong><?php echo $topic['category_name']; ?></strong></small>
        <?php echo word_limiter($topic['topic_body'], 25); ?>
        <p><a class="btn btn-dark" href="<?php echo site_url('/topics/'.$topic['slug']); ?>">Read More</a></p>
    </div>
    </div>
    <?php endforeach; ?>