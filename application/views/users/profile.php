<h1><?= $title; ?></h1>

<?php echo form_open('/users/profile'); ?>
    <div class="form-group">
        <h4>Account details:</h4>
        <h5>Username: <strong><?php echo $user['username']; ?></strong></h5>
        <h5>Email: <strong><?php echo $user['email']; ?></strong></h5>
        <h5>Premission: <strong><?php echo $user['premission']; ?></strong></h5>
        <a class="btn btn-success" href="edit">Edit profile</a>
    </div>
    <div>
        <?php if($user['premission'] == 'superadmin'):?>
            <a class="btn btn-success" href="viewusers">View Users</a>
        <?php endif; ?>
    </div>
</form>
