<?php echo validation_errors(); ?>

<?php echo form_open('/users/update'); ?>
<input type="hidden" name="user_id" value="<?php echo $user['user_id']; ?>">
<div class="h-100 d-flex justify-content-center">
    <div class="col-md-4">
            <h2><?= $title; ?></h2>
            <h4>Change details:</h4>
            <div class="form-group">
                <h5>Username:</h5>
                <input type="text" name="username" class="form-control" placeholder="New username..."  value="<?php echo $user['username']; ?>">
            </div>
            <div class="form-group">
                <h5>Email:</h5>
                <input type="text" name="email" class="form-control" placeholder="New email..." value="<?php echo $user['email']; ?>">
            </div>
            <div class="form-group">
                <h5>New password:</h5>
                <input type="password" name="password" class="form-control" placeholder="New passowrd...">
            </div>
            <button type="submit" class="btn btn-success btn-block">Change</button>
        </div>
    </div>
</form>