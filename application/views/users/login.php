<?php echo form_open('users/login'); ?>
    <div class="h-100 d-flex justify-content-center">
        <div class="col-md-4  text-center">
            <h1 class="text-center"><?php echo $title; ?></h1>
            <div class="form-group">
                <input type="text" name="email" class="form-control" placeholder="Email" required autofocus>
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="password" required autofocus>
            </div>
            <button type="submit" class="btn btn-success btn-block">Login</button>
        </div>
    </div>
</form>