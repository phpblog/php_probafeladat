<h2><?= $title; ?></h2>
<ul class="list-group">
    <?php foreach($categories as $category): ?>
        <li class="list-group-item"><a href="<?php echo site_url('/categories/topics/'.$category['category_id']); ?>"><?php echo $category['category_name']; ?></a></li>
    <?php endforeach; ?>
</ul>