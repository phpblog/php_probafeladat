<?php
    class Users extends CI_Controller
    {
        public function profile()
        {
            if(!$this->session->userdata('logged_in'))
            {
                redirect('users/login');
            }
            $data['title'] ='Account details';

            $user_id = $this->session->userdata('user_id');
            $data['user'] = $this->user_model->get_user($user_id);

            $this->load->view('templates/header');
            $this->load->view('users/profile', $data);
            $this->load->view('templates/footer');
        }

        public function register()
        {
            $data['title'] = 'Sign Up';

            $this->form_validation->set_rules('username', 'Username', 'required|callback_check_username_exists');
            $this->form_validation->set_rules('email', 'Email', 'required|callback_check_email_exists');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('password2', 'Confirm Password', 'matches[password]');

            if($this->form_validation->run() === FALSE)
            {
                $this->load->view('templates/header');
                $this->load->view('users/register', $data);
                $this->load->view('templates/footer');
            }
            else
            {
                $enc_password = md5($this->input->post('password'));
                $this->user_model->register($enc_password);
                
                $this->session->set_flashdata('user_registered', 'You are now registered!');

                redirect('home');
            }
        }

        public function login()
        {
            $data['title'] = 'Sign In';

            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if($this->form_validation->run() === FALSE)
            {
                $this->load->view('templates/header');
                $this->load->view('users/login', $data);
                $this->load->view('templates/footer');
            }
            else
            {
                $email = $this->input->post('email');
                $password = md5($this->input->post('password'));

                $user_id = $this->user_model->login($email, $password);

                if($user_id)
                {
                    $user_data = array(
                        'user_id' => $user_id,
                        'username' => $this->db->select('username'),
                        'premission' => $this->db->select('premission'),
                        'logged_in' => true
                    );

                    $this->session->set_userdata($user_data);

                    $this->session->set_flashdata('user_loggedin', 'You are now logged in!');

                    redirect('home');
                }
                else
                {
                    $this->session->set_flashdata('login_failed', 'Login is invalid.');

                    redirect('users/login');
                }
            }
        }

        public function logout()
        {
            $this->session->unset_userdata('logged_in');
            $this->session->unset_userdata('user_id');
            $this->session->unset_userdata('username');
            $this->session->unset_userdata('premission');
            $this->session->set_flashdata('user_loggedout', 'You are logged out.');
            redirect('users/login');
        }

        public function check_username_exists($username)
        {
            $this->form_validation->set_message('check_username_exists', 'That username is taken. Please choose a diferent one');
            if($this->user_model->check_username_exists($username))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function check_email_exists($email)
        {
            $this->form_validation->set_message('check_email_exists', 'That email is already registered. Please choose a diferent one');
            if($this->user_model->check_email_exists($email))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public function edit()
        {
            if(!$this->session->userdata('logged_in'))
            {
                redirect('users/login');
            }
            $data['title'] ='Edit profile';

            $user_id = $this->session->userdata('user_id');
            $data['user'] = $this->user_model->get_user($user_id);

            $this->load->view('templates/header');
            $this->load->view('users/edit', $data);
            $this->load->view('templates/footer');
        }

        public function update()
        {
            if(!$this->session->userdata('logged_in'))
            {
                redirect('users/login');
            }

            $data['title'] = 'Edit profile';

            $user_id = $this->session->userdata('user_id');
            $data['user'] = $this->user_model->get_user($user_id);

            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[20]');
                
            if($this->form_validation->run() === FALSE)
            {
                $this->load->view('templates/header');
                $this->load->view('users/edit', $data);
                $this->load->view('templates/footer');
            }else
            {
                $password = md5($this->input->post('password'));

                $this->user_model->update_profile($password);
                redirect('/users/profile');
            }
        }

        public function viewusers()
        {
            if(!$this->session->userdata('logged_in'))
            {
                $this->logout();
                redirect('users/login');
            }
            $data['title'] ='Users';

            $user_id = $this->session->userdata('user_id');
            $data['users'] = $this->user_model->get_users($user_id);

            $this->load->view('templates/header');
            $this->load->view('users/viewusers', $data);
            $this->load->view('templates/footer');
        }
    }
?>