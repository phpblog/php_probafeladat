<?php 
    class Categories extends CI_Controller
    {
        public function index()
        {
            $data['title'] = 'Categories';

            $data['categories'] = $this->category_model->get_categories();

            $this->load->view('templates/header');
            $this->load->view('categories/index', $data);
            $this->load->view('templates/footer');
        }

        public function topics($id)
        {
            $data['title']=$this->category_model->get_category($id)->category_name;
            $data['topics'] = $this->topic_model->get_topics_by_category($id);

            $this->load->view('templates/header');
            $this->load->view('topics/index', $data);
            $this->load->view('templates/footer');
        }
    }
?>