<?php 
    class Topics extends CI_Controller
    {
        public function index()
        {
            $data['title'] = 'Latest Topics';

            $data['topics'] = $this->topic_model->get_topics();

            $this->load->view('templates/header');
            $this->load->view('topics/index', $data);
            $this->load->view('templates/footer');
        }
        public function view($slug = NULL)
        {
            $data['topic'] = $this->topic_model->get_topics($slug);
            
            $topic_id = $data['topic']['topic_id'];
            $data['comments'] = $this->comment_model->get_comments($topic_id);

            if(empty($data['topic']))
            {
                show_404();
            }
            $data['title'] = $data['topic']['topic_name'];
            
            $this->load->view('templates/header');
            $this->load->view('topics/view', $data);
            $this->load->view('templates/footer');
        }

        public function create()
        {
            if(!$this->session->userdata('logged_in'))
            {
                redirect('users/login');
            }

            $data['title'] = 'Create Topic';

            $data['categories'] = $this->topic_model->get_categories();

            $this->form_validation->set_rules('topic_name', 'Topic name', 'required');
            $this->form_validation->set_rules('topic_body', 'Topic body', 'required');
            //$this->form_validation->set_rules('topic_date', 'Date', 'required');

            if($this->form_validation->run()===FALSE)
            {
                $this->load->view('templates/header');
                $this->load->view('topics/create', $data);
                $this->load->view('templates/footer');
            }
            else
            {
                $this->topic_model->create_topic();
                redirect('topics');
            }
        }

        public function delete($topic_id)
        {
            if(!$this->session->userdata('logged_in'))
            {
                redirect('users/login');
            }
            $this->topic_model->delete_topic($topic_id);
            redirect('topics');
        }

        public function edit($slug)
        {
            if(!$this->session->userdata('logged_in'))
            {
                redirect('users/login');
            }
            $data['topic'] = $this->topic_model->get_topics($slug);

            if($this->session->userdata('user_id') != $this->topic_model->get_topics($slug)['user_id'])
            {
                redirect('topics');
            }

            $data['categories'] = $this->topic_model->get_categories();

            if(empty($data['topic']))
            {
                show_404();
            }
            $data['title'] = 'Edit topic';
            
            $this->load->view('templates/header');
            $this->load->view('topics/edit', $data);
            $this->load->view('templates/footer');
        }

        public function update()
        {
            if(!$this->session->userdata('logged_in'))
            {
                redirect('users/login');
            }
            $this->topic_model->update_topic();
            redirect('topics');
        }
    }
?>